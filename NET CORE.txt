.NET CORE
**********
1.- instalar netcore https://dotnet.microsoft.com/download

2.- CREAMOS EL PROYECT
dotnet new api -n nombre_proyecto
3.- INGRESAMOS A LA RUTA DEL PROYECTO
cd nombre_proyecto
4.- AGREGAMOS PAQUETES DE CONEXION
dotnet add package Microsoft.EntityFrameworkCore.Design
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
5.- EJECUTAMOS EL PROYECTO
dotnet restore
dotnet build
dotnet run
